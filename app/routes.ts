import Dashboard from './pages/Dashboard.vue';
import Home from './pages/Home.vue';
import Tabs from './pages/Tabs.vue';
import BottomNav from './pages/BottomNav.vue';
import Login from './pages/Login.vue';
import Items from './pages/Items.vue';
import ItemDetails from './pages/ItemDetails.vue';

import Search from './pages/Search.vue';
import Account from './pages/Account.vue';

export const routes = {
  '/home': {
    component: Home
  },
  '/dashboard': {
    component: Dashboard
  },
  '/tabs': {
    component: Tabs
  },
  '/bnav': {
    component: BottomNav
  },
  '/items': {
    component: Items
  },
  '/items-details': {
    component: ItemDetails
  },
  '/search': {
    component: Search
  },
  '/account': {
    component: Account
  },
  '/login': {
    component: Login
  }
};
