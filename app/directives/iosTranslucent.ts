import { isAndroid, isIOS } from 'tns-core-modules/platform';
import { Color } from 'tns-core-modules/color';

export default function iosTranslucent(el) {
  el.nativeView.on('loaded', args => {
    const nsView = args.object;
    const nsColorShadow = new Color('black');

    if (isIOS) {
      let tabBarController: UITabBarController = nsView.ios;
      tabBarController.tabBar.barStyle = UIBarStyle.Default;
    }
  });
}
