import Vue from 'nativescript-vue';

import VueDevtools from 'nativescript-vue-devtools';

import Navigator from 'nativescript-vue-navigator';
import { routes } from './routes';
Vue.use(Navigator, { routes });

import { isLoggedIn } from './helpers';

if (TNS_ENV !== 'production') {
  Vue.use(VueDevtools);
}

import iosTranslucent from './directives/iosTranslucent';
Vue.directive('iosTranslucent', iosTranslucent);

import shadow from './directives/shadow';
Vue.directive('shadow', shadow);

import touchScale from './directives/touchScale';
Vue.directive('touchScale', touchScale);

// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = TNS_ENV === 'production';

new Vue({
  render: h =>
    h('Navigator', {
      attrs: {
        defaultRoute: isLoggedIn() ? '/bnav' : '/login'
      }
    })
}).$start();
