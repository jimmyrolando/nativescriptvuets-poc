import { getBoolean, setBoolean } from 'tns-core-modules/application-settings';

export function login() {
  setBoolean('loggedIn', true);
}

export function isLoggedIn() {
  return getBoolean('loggedIn');
}

export function logout() {
  setBoolean('loggedIn', false);
}
